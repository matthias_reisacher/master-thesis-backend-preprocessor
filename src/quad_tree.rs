use std::{f32, fmt};

use std::cmp::Ordering;
use std::ops::Add;
use BoxResult;

/// Represents a quad tree of a given size.
/// While the max_depth and the element counter are kept up to date, the total area spanned
/// by the tree must be given priorly and is not updated!
pub struct QuadTree {
    root: Box<QuadNode>,
    links: Vec<(u64, u64)>,
    pub n_nodes: u32,
    pub n_links: u32,
    pub max_depth: u8,
}

impl QuadTree {
    /// Creates a new QuadTree
    pub fn new(x_min: f32, x_max: f32, y_min: f32, y_max: f32) -> Self {
        let root = QuadNode::new_root(x_min, x_max, y_min, y_max);
        QuadTree {
            root: Box::new(root),
            links: vec![],
            n_nodes: 0,
            n_links: 0,
            max_depth: 0,
        }
    }

    /// Adds a new point to the root element of this QuadTree.
    pub fn add_node(&mut self, x: f32, y: f32, node_id: u64) -> BoxResult<()> {
        if !self.root.area.inside_area(x, y) {
            return Err(format!("Point ({}, {}) lies not inside the tree's area", x, y).into());
        }

        let id = self.root.add(x, y, node_id)?;

        // Extract the depth information from the child-id and update max_depth if necessary
        let depth = QuadNode::extract_depth_from_id(id);
        if depth > self.max_depth {
            self.max_depth = depth;
        }

        // Update node counter
        self.n_nodes += 1;
        debug!("Added node ({}, {}) to tree", x, y);

        Ok(())
    }

    /// Searches the tree for start and end nodes which match best the passed positions and
    /// adds a new link with the two respective nodes to the list.
    pub fn add_link(
        &mut self,
        start_x: f32,
        start_y: f32,
        end_x: f32,
        end_y: f32,
    ) -> BoxResult<()> {
        let start_id = self.root.get_id_for_position(start_x, start_y)?;
        let end_id = self.root.get_id_for_position(end_x, end_y)?;

        self.links.push((start_id, end_id));

        // Update edge counter
        self.n_links += 1;
        debug!(
            "Added edge ({}, {}) - ({}, {}) to tree",
            start_x, start_y, end_x, end_y
        );

        Ok(())
    }

    /// Get the ID of the node holding the specified position
    #[allow(dead_code)]
    pub fn get_id_for_position(&self, x: f32, y: f32) -> BoxResult<u64> {
        self.root.get_id_for_position(x, y)
    }

    /// Returns a list containing all node of the specified depth.
    /// Additionally, all leaf-nodes at a smaller depth level are added as well.
    pub fn get_nodes_of_depth(&self, depth: u8) -> Vec<SimpleQuadNode> {
        let mut list = Vec::new();
        self.root.collect_nodes_at_depth(depth, &mut list);

        debug!("Found {} nodes for depth {}", list.len(), depth);
        list
    }

    /// Returns a list containing all links represented via the IDs of the start and end points.
    /// All IDs are clipped to the specified depth.
    ///
    /// For instance:
    ///   ID = 4321
    ///   Depth = 2
    ///   Resulting ID = 43
    pub fn get_links_of_depth(&self, depth: u8) -> BoxResult<Vec<(u64, u64)>> {
        let mut list = Vec::new();
        list.reserve(self.links.len());

        // Process all links and clip their id to the given depth
        for link in self.links.iter() {
            let start_id = QuadNode::clip_id_to_depth(link.0, depth);
            let end_id = QuadNode::clip_id_to_depth(link.1, depth);

            match (start_id, end_id) {
                (Ok(start), Ok(end)) => list.push((start, end)),
                _ => warn!("Could not find nodes for link {:?}", link),
            }
        }

        QuadTree::remove_duplicated_edges(&mut list);

        let full_list_size = list.len();
        let n_duplicated_elements = full_list_size - list.len();
        debug!(
            "Found {} edges with {} duplicates for depth {}",
            full_list_size, n_duplicated_elements, depth
        );

        Ok(list)
    }

    /// Removes duplicated edges from the list
    fn remove_duplicated_edges(edges: &mut Vec<(u64, u64)>) {
        edges.sort_by(|(a, b), (c, d)| {
            if a < c {
                Ordering::Less
            } else if a > c {
                Ordering::Greater
            } else {
                // a == c
                if b < d {
                    Ordering::Less
                } else if b > d {
                    Ordering::Greater
                } else {
                    Ordering::Equal
                }
            }
        });
        edges.dedup_by(|(a, b), (c, d)| a == c && b == d);
    }

    /// Returns a readable representation of the quad tree.
    pub fn pretty_print(&self) -> String {
        let mut pretty_str = format!("Quad Tree:\n***** Nodes (# {})*****\n", self.n_nodes);
        self.root.pretty_print(&mut pretty_str, 0);

        pretty_str.push_str(&format!("\n***** Links (# {}) *****\n", self.n_links));
        for (start, end) in self.links.iter() {
            pretty_str.push_str(&format!("- {}, {}\n", start, end));
        }

        pretty_str
    }
}

/// Single node of a QuadTree.
/// Contains the average (x, y)-position of it's children or it's own position if the
/// QuadNode is a leaf.
/// Children nodes are created when a second point is added to the node.
/// Each node consists of a unique id, which represents the path (sub-area id) from the
/// root node to the respective node, written from left to write. For instance, the
/// id = 42 defines the path root > 4. sub-area > 2. sub-area and is located on the second
/// level.
struct QuadNode {
    id: u64,
    x: f32,
    y: f32,
    is_leaf: bool,
    children: [Option<Box<QuadNode>>; 4],
    area: QuadArea,
    node_id: Option<u64>,
}

/// The id of a QuadNode representing a root element
static ROOT_ID: u64 = 0;

impl QuadNode {
    /// Creates a new QuadNode
    fn new(id: u64, x: f32, y: f32, area: QuadArea, node_id: Option<u64>) -> Self {
        let no_childs: [Option<Box<QuadNode>>; 4] =
            [Option::None, Option::None, Option::None, Option::None];

        QuadNode {
            id,
            x,
            y,
            is_leaf: true,
            children: no_childs,
            area,
            node_id,
        }
    }

    /// Creates a new QuadNode which is can be used as a root element
    fn new_root(x_min: f32, x_max: f32, y_min: f32, y_max: f32) -> Self {
        let area = QuadArea::new(x_min, x_max, y_min, y_max);
        let mut root = QuadNode::new(ROOT_ID, 0., 0., area, None);

        // New points added to an empty root should immediately result in a new child
        root.is_leaf = false;

        root
    }

    /// Creates a SimpleQuadNode from this QuadNode
    fn simplify(&self) -> SimpleQuadNode {
        SimpleQuadNode {
            id: self.id,
            x: self.x,
            y: self.y,
            is_leaf: self.is_leaf,
            node_id: self.node_id,
        }
    }

    /// Adds a new child to this node or one of it's child nodes.
    /// Returns the id of the leaf-node holding the added value.
    fn add(&mut self, x: f32, y: f32, node_id: u64) -> BoxResult<u64> {
        // Move position to a child node if node is been a leaf or root
        if self.is_leaf {
            // If node is similar, ignore it!
            if self.x == x && self.y == y {
                let msg = format!("Node ({}, {}) does already exist - ignore new node!", x, y);
                warn!("{}", msg);
                return Err(msg.into());
            }

            let index = self.area.get_sub_area_index(self.x, self.y)?;
            let leaf_pos = (self.x, self.y);
            self.create_new_child(index, leaf_pos.0, leaf_pos.1, self.node_id.unwrap())?;

            self.is_leaf = false;
            self.node_id = None;
        }

        let ret;

        // Add new point to an existing node or create a new one
        let index = self.area.get_sub_area_index(x, y)?;
        if self.children[index].is_some() {
            ret = self.children[index].as_mut().unwrap().add(x, y, node_id);
        } else {
            ret = self.create_new_child(index, x, y, node_id);
        }

        // Recalculate average position of child nodes
        self.calc_avg_pos();

        ret
    }

    /// Creates a new child as a leaf node.
    fn create_new_child(&mut self, index: usize, x: f32, y: f32, node_id: u64) -> BoxResult<u64> {
        let id = self.calc_child_id(index);

        let sub_area = self.area.create_sub_area(index)?;
        let new_child = QuadNode::new(id, x, y, sub_area, Some(node_id));

        self.children[index] = Some(Box::new(new_child));
        Ok(id)
    }

    /// Calculates a child-id from the parent's id and the child's subarea-index.
    /// The child's index increased by 1 is appended to the parent's id.
    /// For instance:
    ///   parent-id = 42 and child-index = 0
    ///   Resulting child-id = 421
    fn calc_child_id(&self, index: usize) -> u64 {
        self.id * 10 + index as u64 + 1
    }

    /// Calculates the average position of it's children.
    fn calc_avg_pos(&mut self) {
        let mut avg = (0., 0.);
        let mut n_childs = 0;

        for child in self.children.as_ref().iter() {
            match child {
                Some(child) => {
                    avg.0 += child.x;
                    avg.1 += child.y;
                    n_childs += 1;
                }
                None => {}
            }
        }
        self.x = avg.0 / n_childs as f32;
        self.y = avg.1 / n_childs as f32;
    }

    /// Calculates and returns the depth level of a node with the given id.
    fn extract_depth_from_id(id: u64) -> u8 {
        f64::log10(10. * id as f64).floor() as u8
    }

    /// Clips the given ID to the specified depth.
    ///
    /// For instance:
    ///   ID = 4321
    ///   Depth = 2
    ///   Resulting ID = 43
    fn clip_id_to_depth(id: u64, depth: u8) -> BoxResult<u64> {
        if depth == 0 {
            return Err("Depth must be larger than zero".into());
        }

        let actual_depth = QuadNode::extract_depth_from_id(id);

        if actual_depth < depth {
            return Ok(id);
        }

        let factor = 10u64.pow((depth as i8 - actual_depth as i8).abs() as u32);
        let clipped_id = id / factor;

        Ok(clipped_id)
    }

    /// Get the ID of the child-node holding the specified position, or the ID of itself.
    fn get_id_for_position(&self, x: f32, y: f32) -> BoxResult<u64> {
        if self.is_leaf {
            return Ok(self.id);
        }

        let index = self.area.get_sub_area_index(x, y)?;
        if self.children[index].is_some() {
            self.children[index]
                .as_ref()
                .unwrap()
                .get_id_for_position(x, y)
        } else {
            Err(format!("No child existing for ({}, {}) in node {}", x, y, self.id).into())
        }
    }

    /// Add the node to the given list if it is a leaf or process the node's children instead.
    /// If a nodes depth is larger than the given depth, the process is stopped.
    fn collect_nodes_at_depth(&self, depth: u8, list: &mut Vec<SimpleQuadNode>) {
        let node_depth = QuadNode::extract_depth_from_id(self.id);

        // Do nothing if node is too deep
        if node_depth > depth {
            return;
        }

        if self.is_leaf || node_depth == depth {
            list.push(self.simplify());
        } else {
            for child in self.children.iter() {
                if let Some(c) = child {
                    c.collect_nodes_at_depth(depth, list);
                }
            }
        }
    }

    /// Returns a readable representation of this node and it's children.
    fn pretty_print(&self, pretty_str: &mut String, depth: u8) {
        for _i in 0..=depth {
            pretty_str.push_str("-");
        }

        let mut pretty_self = format!(" {}: ({}, {}) - []\n", self.id, self.x, self.y);
        if let Some(v_id) = self.node_id {
            pretty_self = pretty_self.add(&format!("[{}]\n", v_id));
        }
        pretty_str.push_str(&pretty_self);

        for child in self.children.as_ref().iter() {
            if let Some(child) = child {
                child.pretty_print(pretty_str, depth + 1);
            }
        }
    }
}

impl fmt::Debug for QuadNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let children = if self.is_leaf {
            String::from("Empty")
        } else {
            let mut content = String::from("");
            for (i, c) in self.children.iter().enumerate() {
                match c {
                    Some(_) => content.push_str("Child"),
                    None => content.push_str("None"),
                };

                if i < self.children.len() - 1 {
                    content.push_str(", ");
                }
            }

            content
        };

        write!(
            f,
            "QuadNode {{id: {}, x: {}, y: {}, children: [{}]}}",
            self.id, self.x, self.y, children
        )
    }
}

/// A simplified version of a QuadNode.
#[derive(Debug)]
pub struct SimpleQuadNode {
    pub id: u64,
    pub x: f32,
    pub y: f32,
    pub is_leaf: bool,
    pub node_id: Option<u64>,
}

/// Defines the area of a single QuadNode.
/// Each area consists of four subareas with (almost) similar size and an index between 0 to 4.
/// The subareas are numbered as followed:
///        y
///   ___  |  ___
///  |   | | |   |
///  | 1 | | | 3 |
///  |___| | |___|
///        |
/// -------+-------- x
///   ___  |  ___
///  |   | | |   |
///  | 0 | | | 2 |
///  |___| | |___|
///
#[derive(Debug)]
struct QuadArea {
    x_min: f32,
    x_max: f32,
    y_min: f32,
    y_max: f32,
}

impl QuadArea {
    /// Creates a new QuadArea
    fn new(x_min: f32, x_max: f32, y_min: f32, y_max: f32) -> Self {
        QuadArea {
            x_min,
            x_max,
            y_min,
            y_max,
        }
    }

    /// Returns true if the given point is inside the quad area, otherwise false.
    fn inside_area(&self, x: f32, y: f32) -> bool {
        return x >= self.x_min && x <= self.x_max && y >= self.y_min && y <= self.y_max;
    }

    /// Returns the index of the respected subarea for a given point (x, y).
    fn get_sub_area_index(&self, x: f32, y: f32) -> BoxResult<usize> {
        if !self.inside_area(x, y) {
            return Err(format!("{:?} does not contain ({}, {})", self, x, y).into());
        }

        let x_mid = (self.x_min + self.x_max) / 2.;
        let y_mid = (self.y_min + self.y_max) / 2.;

        match (x, y) {
            (_, _) if x <= x_mid && y <= y_mid => Ok(0),
            (_, _) if x <= x_mid && y > y_mid => Ok(1),
            (_, _) if x > x_mid && y <= y_mid => Ok(2),
            (_, _) if x > x_mid && y > y_mid => Ok(3),
            (_, _) => unreachable!(),
        }
    }

    /// Returns a new QuadArea with the dimension of the subarea with the respective index.
    fn create_sub_area(&self, index: usize) -> BoxResult<QuadArea> {
        let x_mid = (self.x_min + self.x_max) / 2.;
        let y_mid = (self.y_min + self.y_max) / 2.;

        match index {
            0 => Ok(QuadArea::new(self.x_min, x_mid, self.y_min, y_mid)),
            1 => Ok(QuadArea::new(
                self.x_min,
                x_mid,
                y_mid + f32::MIN_POSITIVE,
                self.y_max,
            )),
            2 => Ok(QuadArea::new(
                x_mid + f32::MIN_POSITIVE,
                self.x_max,
                self.y_min,
                y_mid,
            )),
            3 => Ok(QuadArea::new(
                x_mid + f32::MIN_POSITIVE,
                self.x_max,
                y_mid + f32::MIN_POSITIVE,
                self.y_max,
            )),
            _ => Err("Sub area index must be between 0 and 4".into()),
        }
    }
}

#[cfg(test)]
mod tests_quad_tree {
    use super::*;

    fn create_test_quad_tree() -> QuadTree {
        QuadTree::new(-10., 10., -5., 5.)
    }

    #[test]
    fn new() {
        let tree = create_test_quad_tree();
        assert_eq!(0, tree.root.id);
        assert_eq!(0., tree.root.x);
        assert_eq!(0., tree.root.y);
        assert_eq!(-10., tree.root.area.x_min);
        assert_eq!(10., tree.root.area.x_max);
        assert_eq!(-5., tree.root.area.y_min);
        assert_eq!(5., tree.root.area.y_max);
        assert_eq!(0, tree.n_nodes);
        assert_eq!(0, tree.max_depth);
    }

    #[test]
    fn add_node() {
        let mut tree = create_test_quad_tree();
        let res = tree.add_node(-5., -2.5, 123);
        assert!(res.is_ok());
        assert_eq!(1, tree.max_depth);
        assert_eq!(1, tree.n_nodes);
        assert!(tree.root.children[0].is_some());
        assert!(tree.root.children[1].is_none());
        assert!(tree.root.children[2].is_none());
        assert!(tree.root.children[3].is_none());

        let child_0 = tree.root.children[0].as_ref().unwrap();
        assert_eq!(1, child_0.id);
        assert_eq!(-5., child_0.x);
        assert_eq!(-2.5, child_0.y);
        assert_eq!(123, child_0.node_id.unwrap());
    }

    #[test]
    fn add_node_outside_of_tree_area() {
        let mut tree = create_test_quad_tree();
        let res = tree.add_node(-100., -100., 123);
        assert!(res.is_err());
    }

    #[test]
    fn add_link() {
        let mut tree = create_test_quad_tree();
        tree.add_node(-5., -2.5, 123).unwrap();
        tree.add_node(2., 4., 456).unwrap();
        tree.add_node(1., 2., 789).unwrap();

        let res = tree.add_link(-5., -2.5, 1., 2.);
        assert!(res.is_ok());

        assert_eq!(1, tree.links.len());
        let link = tree.links.get(0).unwrap();
        assert_eq!(1, link.0);
        assert_eq!(41, link.1);
    }

    #[test]
    fn get_links_of_depth() {
        let mut tree = create_test_quad_tree();
        tree.add_node(-5., -2.5, 123).unwrap();
        tree.add_node(2., 4., 456).unwrap();
        tree.add_node(1., 2., 789).unwrap();
        tree.add_link(-5., -2.5, 2., 4.).unwrap();
        tree.add_link(-5., -2.5, 1., 2.).unwrap();

        let links = tree.get_links_of_depth(0).unwrap();
        assert_eq!(0, links.len());

        let links = tree.get_links_of_depth(1).unwrap();
        assert_eq!(1, links.len());
        assert_eq!((1, 4), *links.get(0).unwrap());

        let links = tree.get_links_of_depth(2).unwrap();
        assert_eq!(2, links.len());
        assert_eq!((1, 41), *links.get(0).unwrap());
        assert_eq!((1, 42), *links.get(1).unwrap());
    }

    #[test]
    fn remove_duplicated_edges() {
        let mut edges: Vec<(u64, u64)> = Vec::new();
        edges.push((1, 2));
        edges.push((1, 3));
        edges.push((2, 1));
        edges.push((2, 1));
        edges.push((1, 2));

        QuadTree::remove_duplicated_edges(&mut edges);

        assert_eq!(edges.len(), 3);
        assert_eq!(edges.get(0), Some(&(1, 2)));
        assert_eq!(edges.get(1), Some(&(1, 3)));
        assert_eq!(edges.get(2), Some(&(2, 1)));
    }
}

#[cfg(test)]
mod tests_quad_node {
    use super::*;

    fn create_test_quad_node(x: f32, y: f32) -> QuadNode {
        assert!(x <= 2.);
        assert!(x >= -2.);
        assert!(y <= 2.);
        assert!(y >= -2.);

        let area = QuadArea::new(-2., 2., -2., 2.);
        QuadNode::new(42, x, y, area, Some(123))
    }

    fn create_test_root_node() -> QuadNode {
        QuadNode::new_root(-10., 10., -5., 5.)
    }

    #[test]
    fn new() {
        let node = create_test_quad_node(1., 2.);
        assert_eq!(42, node.id);
        assert_eq!(1., node.x);
        assert_eq!(2., node.y);
        assert!(node.is_leaf);
        assert_eq!(123, node.node_id.unwrap());
        assert_eq!(4, node.children.len());
        assert!(node.children[0].is_none());
        assert!(node.children[1].is_none());
        assert!(node.children[2].is_none());
        assert!(node.children[3].is_none());
    }

    #[test]
    fn new_root() {
        let root = create_test_root_node();
        assert_eq!(ROOT_ID, root.id);
        assert_eq!(0., root.x);
        assert_eq!(0., root.y);
        assert!(!root.is_leaf);
        assert!(root.node_id.is_none());
        assert_eq!(4, root.children.len());
        assert!(root.children[0].is_none());
        assert!(root.children[1].is_none());
        assert!(root.children[2].is_none());
        assert!(root.children[3].is_none());
    }

    #[test]
    fn create_new_child() {
        let mut node = create_test_quad_node(1., 2.);
        node.create_new_child(0, 0., -1., 456).unwrap();
        assert!(node.children[0].is_some());
        assert!(node.children[1].is_none());
        assert!(node.children[2].is_none());
        assert!(node.children[3].is_none());

        let child_0 = node.children[0].as_ref().unwrap();
        assert_eq!(421, child_0.id);
        assert_eq!(-0., child_0.x);
        assert_eq!(-1., child_0.y);
        assert!(child_0.is_leaf);
        assert_eq!(456, child_0.node_id.unwrap());
        assert_eq!(-2., child_0.area.x_min);
        assert_eq!(-0., child_0.area.x_max);
        assert_eq!(-2., child_0.area.y_min);
        assert_eq!(-0., child_0.area.y_max);
    }

    #[test]
    fn add_point_to_root_node() {
        let mut root = create_test_root_node();
        let id = root.add(5., 2.5, 123);
        assert_eq!(4, id.unwrap());

        assert_eq!(5., root.x);
        assert_eq!(2.5, root.y);
        assert!(root.node_id.is_none());
        assert!(root.children[0].is_none());
        assert!(root.children[1].is_none());
        assert!(root.children[2].is_none());
        assert!(root.children[3].is_some());

        let child_3 = root.children[3].as_ref().unwrap();
        assert_eq!(4, child_3.id);
        assert_eq!(5., child_3.x);
        assert_eq!(2.5, child_3.y);
        assert!(child_3.is_leaf);
        assert_eq!(123, child_3.node_id.unwrap());
    }

    #[test]
    fn add_point_to_empty_quad_node() {
        let mut node = create_test_quad_node(1., 2.);
        let id = node.add(-1., -2., 456);
        assert_eq!(421, id.unwrap());
        assert!(node.node_id.is_none());
        assert!(node.children[0].is_some());
        assert!(node.children[1].is_none());
        assert!(node.children[2].is_none());
        assert!(node.children[3].is_some());

        let child_0 = node.children[0].as_ref().unwrap();
        assert_eq!(421, child_0.id);
        assert_eq!(-1., child_0.x);
        assert_eq!(-2., child_0.y);
        assert_eq!(456, child_0.node_id.unwrap());
        assert!(child_0.is_leaf);
        assert_eq!(-2., child_0.area.x_min);
        assert_eq!(-0., child_0.area.x_max);
        assert_eq!(-2., child_0.area.y_min);
        assert_eq!(-0., child_0.area.y_max);
        assert!(child_0.children[0].is_none());
        assert!(child_0.children[1].is_none());
        assert!(child_0.children[2].is_none());
        assert!(child_0.children[3].is_none());

        let child_3 = node.children[3].as_ref().unwrap();
        assert_eq!(424, child_3.id);
        assert_eq!(123, child_3.node_id.unwrap());
    }

    #[test]
    fn add_points_to_non_empty_quad_node() {
        let mut node = create_test_quad_node(1., 2.);
        assert_eq!(421, node.add(-1., -2., 1).unwrap());
        assert_eq!(423, node.add(1., -2., 2).unwrap());
        assert_eq!(422, node.add(-1., 2., 3).unwrap());
        assert!(node.children[0].is_some());
        assert!(node.children[1].is_some());
        assert!(node.children[2].is_some());
        assert!(node.children[3].is_some());
    }

    #[test]
    fn add_points_to_inner_child() {
        let mut node = create_test_quad_node(1., 2.);
        node.add(-2., -2., 456).unwrap();
        node.add(-0.5, -2., 789).unwrap();
        assert!(node.children[0].is_some());
        assert!(node.children[1].is_none());
        assert!(node.children[2].is_none());
        assert!(node.children[3].is_some());

        let child_0 = node.children[0].as_ref().unwrap();
        assert_eq!(421, child_0.id);
        assert_eq!(-1.25, child_0.x);
        assert_eq!(-2., child_0.y);
        assert!(!child_0.is_leaf);
        assert!(child_0.node_id.is_none());
        assert!(child_0.children[0].is_some());
        assert!(child_0.children[1].is_none());
        assert!(child_0.children[2].is_some());
        assert!(child_0.children[3].is_none());
        assert_eq!(-2., child_0.area.x_min);
        assert_eq!(0., child_0.area.x_max);
        assert_eq!(-2., child_0.area.y_min);
        assert_eq!(0., child_0.area.y_max);

        let child_3 = node.children[3].as_ref().unwrap();
        assert_eq!(123, child_3.node_id.unwrap());
    }

    #[test]
    fn add_similar_point() {
        let mut node = create_test_quad_node(0., 0.);
        let res = node.add(0., 0., 456);
        assert!(res.is_err());
    }

    #[test]
    fn calc_child_id() {
        let node = create_test_quad_node(0., 0.);
        assert_eq!(421, node.calc_child_id(0));
        assert_eq!(422, node.calc_child_id(1));
        assert_eq!(423, node.calc_child_id(2));
        assert_eq!(424, node.calc_child_id(3));
    }

    #[test]
    fn calc_avg_pos_during_add() {
        let mut node = create_test_quad_node(2., 0.75);
        node.add(-2., -1., 456).unwrap();
        node.add(-0.5, -0.75, 789).unwrap();

        assert_eq!(0.375, node.x);
        assert_eq!(-0.0625, node.y);

        let child_0 = node.children[0].as_ref().unwrap();
        assert_eq!(-1.25, child_0.x);
        assert_eq!(-0.875, child_0.y);

        let child_3 = node.children[3].as_ref().unwrap();
        assert_eq!(2., child_3.x);
        assert_eq!(0.75, child_3.y);
    }

    #[test]
    fn extract_depth_from_id() {
        assert_eq!(0, QuadNode::extract_depth_from_id(0));
        assert_eq!(1, QuadNode::extract_depth_from_id(1));
        assert_eq!(2, QuadNode::extract_depth_from_id(43));
        assert_eq!(6, QuadNode::extract_depth_from_id(433241));
    }

    #[test]
    fn clip_id_to_depth() {
        assert!(QuadNode::clip_id_to_depth(4321, 0).is_err());
        assert_eq!(4, QuadNode::clip_id_to_depth(4321, 1).unwrap());
        assert_eq!(43, QuadNode::clip_id_to_depth(4321, 2).unwrap());
        assert_eq!(432, QuadNode::clip_id_to_depth(4321, 3).unwrap());
        assert_eq!(4321, QuadNode::clip_id_to_depth(4321, 4).unwrap());
        assert_eq!(4321, QuadNode::clip_id_to_depth(4321, 5).unwrap());
    }

    #[test]
    fn get_id_for_position_from_root() {
        let node = create_test_root_node();
        let id = node.get_id_for_position(1., 1.);
        assert!(id.is_err());
    }

    #[test]
    fn get_id_for_position_from_leaf() {
        let node = create_test_quad_node(2., 0.75);
        let id = node.get_id_for_position(1., 1.);
        assert_eq!(42, id.unwrap());
    }

    #[test]
    fn get_id_for_position_from_child() {
        let mut node = create_test_quad_node(2., 0.75);
        node.add(-2., -1., 456).unwrap();

        let id = node.get_id_for_position(-1., -1.);
        assert_eq!(421, id.unwrap());
    }

    #[test]
    fn get_id_for_position_from_non_existing_child() {
        let mut node = create_test_quad_node(2., 0.75);
        node.add(-2., -1., 456).unwrap();

        let id = node.get_id_for_position(-1., 1.);
        assert!(id.is_err());
    }

    #[test]
    fn collect_nodes_at_depth() {
        let mut node = create_test_root_node();
        node.add(2., 0.75, 11).unwrap();
        node.add(-2., -1., 22).unwrap();
        node.add(-0.5, -0.75, 33).unwrap();
        node.add(-0.25, -0.5, 44).unwrap();

        let mut nodes_for_depth_4 = Vec::new();
        node.collect_nodes_at_depth(4, &mut nodes_for_depth_4);
        assert_eq!(4, nodes_for_depth_4.len());
        assert_eq!(1441, nodes_for_depth_4.get(0).unwrap().id);
        assert_eq!(1443, nodes_for_depth_4.get(1).unwrap().id);
        assert_eq!(1444, nodes_for_depth_4.get(2).unwrap().id);
        assert_eq!(4, nodes_for_depth_4.get(3).unwrap().id);

        let mut nodes_for_depth_3 = Vec::new();
        node.collect_nodes_at_depth(3, &mut nodes_for_depth_3);
        assert_eq!(2, nodes_for_depth_3.len());
        assert_eq!(144, nodes_for_depth_3.get(0).unwrap().id);
        assert_eq!(4, nodes_for_depth_3.get(1).unwrap().id);

        let mut nodes_for_depth_2 = Vec::new();
        node.collect_nodes_at_depth(2, &mut nodes_for_depth_2);
        assert_eq!(2, nodes_for_depth_2.len());
        assert_eq!(14, nodes_for_depth_2.get(0).unwrap().id);
        assert_eq!(4, nodes_for_depth_2.get(1).unwrap().id);

        let mut nodes_for_depth_1 = Vec::new();
        node.collect_nodes_at_depth(1, &mut nodes_for_depth_1);
        assert_eq!(2, nodes_for_depth_1.len());
        assert_eq!(1, nodes_for_depth_1.get(0).unwrap().id);
        assert_eq!(4, nodes_for_depth_1.get(1).unwrap().id);
    }
}

#[cfg(test)]
mod tests_quad_area {
    use super::*;

    #[test]
    fn get_sub_area_index() {
        let area = QuadArea::new(-3., 3., -2.5, 2.5);

        let sub_0 = area.get_sub_area_index(-3., -2.5).unwrap();
        assert_eq!(0, sub_0);

        let sub_1 = area.get_sub_area_index(0., 0.1).unwrap();
        assert_eq!(1, sub_1);

        let sub_2 = area.get_sub_area_index(0.1, 0.).unwrap();
        assert_eq!(2, sub_2);

        let sub_3 = area.get_sub_area_index(3., 2.5).unwrap();
        assert_eq!(3, sub_3);

        let sub_err_0 = area.get_sub_area_index(-3.1, 0.);
        assert!(sub_err_0.is_err());

        let sub_err_1 = area.get_sub_area_index(3.1, 0.);
        assert!(sub_err_1.is_err());

        let sub_err_2 = area.get_sub_area_index(0., -2.6);
        assert!(sub_err_2.is_err());

        let sub_err_3 = area.get_sub_area_index(0., 2.6);
        assert!(sub_err_3.is_err());
    }

    #[test]
    fn create_sub_area() {
        let area = QuadArea::new(-3., 3., -2.5, 2.5);

        let sub_0 = area.create_sub_area(0).unwrap();
        assert_eq!(-3., sub_0.x_min);
        assert_eq!(0., sub_0.x_max);
        assert_eq!(-2.5, sub_0.y_min);
        assert_eq!(0., sub_0.y_max);

        let sub_1 = area.create_sub_area(1).unwrap();
        assert_eq!(-3., sub_1.x_min);
        assert_eq!(0., sub_1.x_max);
        assert_eq!(0. + f32::MIN_POSITIVE, sub_1.y_min);
        assert_eq!(2.5, sub_1.y_max);

        let sub_2 = area.create_sub_area(2).unwrap();
        assert_eq!(0. + f32::MIN_POSITIVE, sub_2.x_min);
        assert_eq!(3., sub_2.x_max);
        assert_eq!(-2.5, sub_2.y_min);
        assert_eq!(0., sub_2.y_max);

        let sub_3 = area.create_sub_area(3).unwrap();
        assert_eq!(0. + f32::MIN_POSITIVE, sub_3.x_min);
        assert_eq!(3., sub_3.x_max);
        assert_eq!(0. + f32::MIN_POSITIVE, sub_3.y_min);
        assert_eq!(2.5, sub_3.y_max);

        let sub_err = area.create_sub_area(5);
        assert!(sub_err.is_err());
    }
}
