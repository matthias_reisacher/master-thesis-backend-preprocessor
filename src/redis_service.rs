use std::env;

use BoxResult;

use super::redis::{Client, Commands};

use self::models::*;

static ENV_REDIS_HOST: &str = "REDIS_HOST";
static ENV_REDIS_PORT: &str = "REDIS_PORT";

pub struct RedisService {
    client: Client,
}

impl RedisService {
    /// Creates a new redis service.
    pub fn new() -> BoxResult<Self> {
        let url = create_url()?;
        debug!("Connecting to Redis via '{}'", url);

        let client =
            Client::open(url.as_str()).map_err(|e| format!("Could not connect to Redis: {}", e))?;

        Ok(RedisService { client })
    }

    /// Fetches a graph page consisting of metadata and file content with the given id.
    pub fn fetch_graph_page_file(&self, id: &str) -> BoxResult<GraphPageFile> {
        let con = self.client.get_connection()?;
        let mut graph_page: GraphPageFile = con.get(format!("file:{}:metadata", id))?;
        graph_page.content = con.get(format!("file:{}:content", id))?;

        debug!(
            "Fetched graph page file:\nid={}, graph id={}, name={}, format={}, size={}\n{}\n",
            graph_page.id,
            graph_page.graph_id,
            graph_page.name,
            graph_page.format,
            graph_page.size,
            graph_page.content
        );
        Ok(graph_page)
    }

    /// Stores a layout page with the respective id.
    pub fn put_layout_page(&self, file: &LayoutFile) -> BoxResult<()> {
        let con = self.client.get_connection()?;
        con.set(format!("layout:{}:raw", file.id), &file.content)?;
        debug!("Stored layout file with id {} in redis", file.id);
        Ok(())
    }

    /// Removes a layout page with the given id.
    pub fn delete_layout_page(&self, id: &str) -> BoxResult<()> {
        let con = self.client.get_connection()?;
        con.del(format!("layout:{}:raw", id))?;
        debug!("Removed layout file with id {} from redis", id);
        Ok(())
    }

    /// Fetches a layout result with the given id.
    pub fn fetch_layout_result(&self, id: &str) -> BoxResult<LayoutFile> {
        let mut layout_file = LayoutFile::new(id.to_string());
        let con = self.client.get_connection()?;
        layout_file.content = con.get(format!("layout:{}:processed", id))?;

        debug!(
            "Fetched layout file:\nid={}\n{}\n",
            layout_file.id, layout_file.content
        );
        Ok(layout_file)
    }

    /// Removes a layout result with the given id.
    pub fn delete_layout_result(&self, id: &str) -> BoxResult<()> {
        let con = self.client.get_connection()?;
        con.del(format!("layout:{}:processed", id))?;
        debug!("Removed layout result with id {} from redis", id);
        Ok(())
    }
}

impl Clone for RedisService {
    fn clone(&self) -> Self {
        RedisService::new().expect("Error cloning redis service")
    }
}

/// Creates a URL to the redis service in the format:
/// "redis://<user>:<password>@<endpoint>:<port>/"
fn create_url() -> BoxResult<String> {
    if let (Ok(host), Ok(port)) = (env::var(ENV_REDIS_HOST), env::var(ENV_REDIS_PORT)) {
        Ok(format!("redis://{}:{}/", host, port))
    } else {
        Err("Could not parse environment variable for redis connection".into())
    }
}

pub mod models {
    use std::str;

    use redis::{ErrorKind, FromRedisValue, RedisError, Value};

    /// Redis model containing the file data of an individual graph page.
    #[derive(Debug)]
    pub struct GraphPageFile {
        pub id: String,
        pub graph_id: i64,
        pub name: String,
        pub format: String,
        pub size: u64,
        pub content: String,
    }

    impl FromRedisValue for GraphPageFile {
        fn from_redis_value(v: &Value) -> Result<Self, RedisError> {
            let empty_string = String::from("");
            let data = match v {
                Value::Data(v) => str::from_utf8(v).unwrap(),
                Value::Status(s) => s,
                _ => &empty_string,
            };

            let json_data = json::parse(data).unwrap();

            if let (Some(id), Some(graph_id), Some(name), Some(format), Some(size)) = (
                json_data["id"].as_str(),
                json_data["graphId"].as_i64(),
                json_data["name"].as_str(),
                json_data["type"].as_str(),
                json_data["size"].as_u64(),
            ) {
                let metadata = GraphPageFile {
                    id: id.to_string(),
                    graph_id,
                    name: name.to_string(),
                    format: format.to_string(),
                    size,
                    content: String::from(""),
                };

                Ok(metadata)
            } else {
                Err(RedisError::from((
                    ErrorKind::IoError,
                    "Could not map all JSON-values to instantiate a GraphPageFile",
                )))
            }
        }

        fn from_redis_values(_items: &[Value]) -> Result<Vec<Self>, RedisError> {
            unimplemented!()
        }

        fn from_byte_vec(_vec: &[u8]) -> Option<Vec<Self>> {
            unimplemented!()
        }
    }

    /// Redis model containing the file and id of an individual layout file.
    #[derive(Debug)]
    pub struct LayoutFile {
        pub id: String,
        pub content: String,
    }

    impl LayoutFile {
        pub fn new(id: String) -> Self {
            let content = "".to_string();

            LayoutFile { id, content }
        }

        pub fn new_with_random_id() -> Self {
            let id = nanoid::simple();
            let content = "".to_string();

            LayoutFile { id, content }
        }
    }
}
