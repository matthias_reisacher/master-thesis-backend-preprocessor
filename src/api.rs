use futures::Future;
use grpcio::{RpcContext, UnarySink};

use benchmark::Bencher;
use data_preprocessor::Preprocessor;
use grpc::process_file::{ProcessFileRequest, ProcessFileResponse};
use grpc::process_file_grpc::ProcessFileService;
use BoxResult;

#[derive(Clone)]
pub struct ProcessFileServiceImpl {
    preprocessor: Preprocessor,
    bencher: Bencher,
}

impl ProcessFileServiceImpl {
    // Create new ProcessFileServiceImpl
    pub fn new() -> BoxResult<Self> {
        let preprocessor = Preprocessor::new()?;

        Ok(ProcessFileServiceImpl {
            preprocessor,
            bencher: Bencher::new(),
        })
    }
}

impl ProcessFileService for ProcessFileServiceImpl {
    fn process(
        &mut self,
        ctx: RpcContext,
        req: ProcessFileRequest,
        sink: UnarySink<ProcessFileResponse>,
    ) {
        info!("Received process-request: {:?}", req);
        self.bencher.start();

        let result = self
            .preprocessor
            .process_graph_file(&req.id)
            .map_err(|e| error!("Could not process received request: {}", e));
        info!("Processing of file with id={} completed!", req.id);

        let mut response = ProcessFileResponse::new();
        response.set_result(result.is_ok());

        self.bencher.measure("Total processing time");

        let f = sink
            .success(response)
            .map_err(move |e| error!("Failed to reply: {:?}: {:?}", req, e));
        ctx.spawn(f);
    }
}
