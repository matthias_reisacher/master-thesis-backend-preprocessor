use std::env;

use postgres::{rows::Rows, Connection, TlsMode};

use postgres::transaction::Transaction;
use psql_service::models::{Attribute, Edge, Graph, Node, Page, Position};
use BoxResult;

static ENV_PSQL_HOST: &str = "POSTGRES_HOST";
static ENV_PSQL_PORT: &str = "POSTGRES_PORT";
static ENV_PSQL_DB: &str = "POSTGRES_DB";
static ENV_PSQL_USER: &str = "POSTGRES_USER";
static ENV_PSQL_PWD: &str = "POSTGRES_PASSWORD";

pub struct PsqlService {
    conn: Connection,
}

impl PsqlService {
    pub fn new() -> BoxResult<Self> {
        let url = create_url()?;
        debug!("Connecting to Postgres via '{}'", url);

        let conn = Connection::connect(url.as_str(), TlsMode::None)
            .map_err(|e| format!("Could not connect to Postgres: {}", e))?;

        Ok(PsqlService { conn })
    }

    /// Returns the graph with the given id
    #[allow(dead_code)]
    pub fn get_graph(&self, id: i64) -> BoxResult<Graph> {
        self.conn
            .query("SELECT * FROM graph WHERE graph_id = $1", &[&id])
            .map_err(|e| format!("Could not fetching graph with id = {}: {}", id, e).into())
            .and_then(|rows| {
                let row = rows.get(0);
                let graph = Graph {
                    id: row.get(0),
                    name: row.get(1),
                    description: row.get(2),
                    timestamp: row.get(3),
                };
                Ok(graph)
            })
    }

    /// Returns the largest number of a graph page found for the given page.
    pub fn get_num_graph_pages(&self, graph_id: i64) -> BoxResult<i16> {
        self.conn
            .query(
                "SELECT MAX(number) FROM page WHERE graph_id = $1",
                &[&graph_id],
            )
            .map_err(|e| {
                format!(
                    "Error while fetching max page-number of graph with id = {}: {}",
                    graph_id, e
                )
                .into()
            })
            .and_then(|rows| {
                let max: Option<i16> = rows.get(0).get(0);
                Ok(max.unwrap_or(0))
            })
    }

    /// Inserts the given page into the database and returns the created ID of the page.
    pub fn insert_graph_page(&self, page: &Page) -> BoxResult<i64> {
        debug!("Inserting graph page: {:?}", page);
        self.conn.query("INSERT INTO page (graph_id, number, max_depth, timestamp) VALUES ($1, $2, $3, $4) RETURNING id",
                        &[&page.graph_id, &page.number, &page.max_depth, &page.timestamp])
            .map_err(|e| format!("Error while inserting graph page. {}", e).into())
            .and_then(|rows| get_id_from_rows(rows))
    }

    /// Inserts the node and it's position into the database and stores the created IDs in the
    /// given node structure.
    pub fn insert_node(
        &self,
        node: &mut Node,
        position: &Position,
        attribute: &Attribute,
    ) -> BoxResult<()> {
        debug!(
            "Insert node {:?} at position {:?} with attribute {:?}",
            node, position, attribute
        );
        let mut success = true;
        let trans = self.conn.transaction()?;

        let position_id: BoxResult<i64> = trans
            .query(
                "INSERT INTO position (x, y) VALUES ($1, $2) RETURNING id",
                &[&position.x, &position.y],
            )
            .map_err(|e| e.into())
            .and_then(|rows| get_id_from_rows(rows))
            .map_err(|e| {
                error!("Error while inserting position {:?}: {}", position, e);
                e.into()
            });

        match position_id {
            Ok(position_id) => {
                match store_attribute(&trans, &attribute) {
                    Ok(attribute_id) => {
                        let node_id: BoxResult<i64> =
                            trans.query("INSERT INTO node (page_id, depth, leaf, position_id, attribute_id) VALUES ($1, $2, $3, $4, $5) RETURNING id",
                                        &[&node.page_id, &node.depth, &node.leaf, &position_id, &attribute_id])
                                 .map_err(|e| e.into())
                                 .and_then(|rows| get_id_from_rows(rows))
                                 .map_err(|e| {
                                     error!("Error while inserting node {:?}: {}", node, e);
                                     e.into()
                                 });

                        match node_id {
                            Ok(node_id) => {
                                node.id = node_id;
                                node.position_id = position_id;

                                trans.set_commit();
                            }
                            Err(_) => {
                                trans.set_rollback();
                                success = false;
                            }
                        }
                    }
                    Err(_) => {
                        trans.set_rollback();
                        success = false;
                    }
                };
            }
            Err(_) => {
                trans.set_rollback();
                success = false;
            }
        };

        trans.finish()?;

        match success {
            true => Ok(()),
            false => Err("Could not persist node".into()),
        }
    }

    /// Inserts the given edge into the database and returns the created ID of the edge.
    /// Assumes that the start and end positions are already existing inside the database.
    pub fn insert_edge(&self, edge: &mut Edge, attribute: &Attribute) -> BoxResult<()> {
        debug!("Insert edge {:?} with attribute {:?}", edge, attribute);
        let mut success = true;
        let trans = self.conn.transaction()?;

        match store_attribute(&trans, &attribute) {
            Ok(attribute_id) => {
                let id: BoxResult<i64> =
                    trans.query("INSERT INTO edge (page_id, depth, start_node_id, end_node_id, attribute_id) VALUES ($1, $2, $3, $4, $5) RETURNING id",
                                &[&edge.page_id, &edge.depth, &edge.start_node_id, &edge.end_node_id, &attribute_id])
                         .map_err(|e| e.into())
                         .and_then(|rows| get_id_from_rows(rows))
                         .map_err(|e| {
                             error!("Error while inserting edge {:?}: {}", edge, e);
                             e.into()
                         });

                match id {
                    Ok(id) => {
                        edge.id = id;
                        trans.set_commit();
                    }
                    Err(_) => {
                        trans.set_rollback();
                        success = false;
                    }
                }
            }
            Err(_) => {
                trans.set_rollback();
                success = false;
            }
        };

        trans.finish()?;

        match success {
            true => Ok(()),
            false => Err("Could not persist edge".into()),
        }
    }
}

impl Clone for PsqlService {
    fn clone(&self) -> Self {
        PsqlService::new().expect("Error cloning psql service")
    }
}

/// Extract the ID from the Rows object returned by the database.
/// Assumes that the ID is located at the first row.
fn get_id_from_rows(rows: Rows) -> BoxResult<i64> {
    let id: Option<i64> = rows.get(0).get(0);
    id.ok_or("Invalid ID returned during graph page insertion".into())
}

fn store_attribute(trans: &Transaction, attribute: &Attribute) -> BoxResult<i64> {
    trans.query(
            "INSERT INTO attribute (data_id, quad_tree, attributes, lifetime) VALUES ($1, $2, $3, $4) RETURNING id",
            &[&attribute.data_id, &attribute.quad_tree_id, &attribute.attributes, &attribute.lifetime],
        )
        .map_err(|e| e.into())
        .and_then(|rows| get_id_from_rows(rows))
        .map_err(|e| {
            error!("Error while inserting attribute {:?}: {}", attribute, e);
            e.into()
        })
}

/// Creates a URL to the redis service in the format:
/// "redis://<user>:<password>@<endpoint>:<port>/<database>"
fn create_url() -> BoxResult<String> {
    if let (Ok(host), Ok(port), Ok(db), Ok(user), Ok(pwd)) = (
        env::var(ENV_PSQL_HOST),
        env::var(ENV_PSQL_PORT),
        env::var(ENV_PSQL_DB),
        env::var(ENV_PSQL_USER),
        env::var(ENV_PSQL_PWD),
    ) {
        Ok(format!(
            "postgres://{}:{}@{}:{}/{}",
            user, pwd, host, port, db
        ))
    } else {
        Err("Could not parse environment variable for postgres connection".into())
    }
}

/// Contains all needed models of the DAGO-database.
pub mod models {
    use chrono::{NaiveDateTime, Utc};

    #[derive(Debug)]
    pub struct Graph {
        pub id: i64,
        pub name: String,
        pub description: String,
        pub timestamp: NaiveDateTime,
    }

    #[allow(dead_code)]
    impl Graph {
        pub fn new(id: i64, name: String, description: String) -> Self {
            Graph {
                id,
                name,
                description,
                timestamp: Utc::now().naive_utc(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Page {
        pub id: i64,
        pub graph_id: i64,
        pub number: i16,
        pub max_depth: i16,
        pub timestamp: NaiveDateTime,
    }

    impl Page {
        pub fn new(id: i64, graph_id: i64, number: i16, max_depth: i16) -> Self {
            Page {
                id,
                graph_id,
                number,
                max_depth,
                timestamp: Utc::now().naive_utc(),
            }
        }
    }

    #[derive(Debug)]
    pub struct Node {
        pub id: i64,
        pub page_id: i64,
        pub depth: i16,
        pub leaf: bool,
        pub position_id: i64,
        pub attribute_id: i64,
    }

    #[derive(Debug)]
    pub struct Edge {
        pub id: i64,
        pub page_id: i64,
        pub depth: i16,
        pub start_node_id: i64,
        pub end_node_id: i64,
        pub attribute_id: i64,
    }

    #[derive(Debug)]
    pub struct Position {
        pub id: i64,
        pub x: f32,
        pub y: f32,
    }

    #[derive(Debug)]
    pub struct Attribute {
        pub id: i64,
        pub data_id: Option<i64>,
        pub quad_tree_id: i64,
        pub attributes: serde_json::Value,
        pub lifetime: i16,
    }

    impl Attribute {
        pub fn new(id: i64, data_id: Option<i64>, quad_tree_id: i64) -> Self {
            Attribute {
                id,
                data_id,
                quad_tree_id,
                attributes: serde_json::Value::default(),
                lifetime: 0,
            }
        }

        pub fn set_attributes(&mut self, attributes: &str) {
            self.attributes = serde_json::Value::from(attributes);
        }
    }

    impl Default for Attribute {
        fn default() -> Self {
            let mut attribute = Attribute {
                id: 0,
                data_id: None,
                quad_tree_id: 0,
                attributes: Default::default(),
                lifetime: 0,
            };

            attribute.set_attributes("{}");

            attribute
        }
    }
}
