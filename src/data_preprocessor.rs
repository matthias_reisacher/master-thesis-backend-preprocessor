use std::collections::{HashMap, LinkedList};
use std::env;
use std::f32;

use benchmark::Bencher;
use data_converter::*;
use grpc::client_layout::LayoutClient;
use grpc::client_sequencer::SequencerClient;
use psql_service::models::{Attribute, Edge, Node, Page, Position};
use psql_service::PsqlService;
use quad_tree::{QuadTree, SimpleQuadNode};
use redis_service::models::LayoutFile;
use redis_service::RedisService;
use BoxResult;

static ENV_SCALE_FACTOR: &str = "SCALE_FACTOR";

#[derive(Clone)]
pub struct Preprocessor {
    redis: RedisService,
    psql: PsqlService,
    scale_factor: u16,
    bencher: Bencher,
}

impl Preprocessor {
    /// Creates a new Preprocessor.
    pub fn new() -> BoxResult<Self> {
        let redis = RedisService::new()?;
        let psql = PsqlService::new()?;

        let scale_factor = match env::var(ENV_SCALE_FACTOR) {
            Ok(arg) => arg.parse::<u16>().unwrap_or(1u16),
            Err(_) => 1u16,
        };

        Ok(Preprocessor {
            redis,
            psql,
            scale_factor,
            bencher: Bencher::new(),
        })
    }

    /// Converts the input file with the given ID into a neutral format, passes it to the
    /// dago-layout-service to generate node positions, simplifies the graph via a quad tree
    ///structure and finally stores every tree level into the database as a new graph page.
    pub fn process_graph_file(&mut self, redis_file_id: &str) -> BoxResult<()> {
        self.bencher.start();

        // Fetch input file from redis
        let file = self.redis.fetch_graph_page_file(redis_file_id)?;
        self.bencher
            .log(&format!("Processing graph : {}", file.name));

        // Parse input file
        let (mut nodes, edges) = parse_input_file(&file.content)?;
        self.bencher.log(&format!("Nodes : {}", nodes.len()));
        self.bencher.log(&format!("Edges : {}", edges.len()));

        // Create input file for the dago-layout-service
        let mut graph_file = LayoutFile::new_with_random_id();
        graph_file.content = create_layout_input_file(&nodes, &edges);

        // Store graph file in redis for dago-layout-service
        self.redis.put_layout_page(&graph_file)?;

        self.bencher.measure("Pre-Layout");

        let mut page_id = -1;

        info!("Sending message to dago-layout-service...");
        LayoutClient::new()
            .map_err(|e| e.into())
            .and_then(|client| {
                self.bencher.start();

                // Send process request to dago-layout-service
                client.process_file(&graph_file.id)?;
                info!("... successfully processed by dago-layout-service.");
                self.bencher.measure("Layout");

                // Fetch output file and remove layout page from redis
                let layout_file = self.redis.fetch_layout_result(&graph_file.id)?;
                self.redis.delete_layout_page(&graph_file.id)?;

                // Parse output file
                let mut layout_nodes = parse_layout_file(&layout_file.content);

                // Move attributes into layouted node data
                swap_attributes(&mut layout_nodes, &mut nodes);

                // Scale layout
                scale_layout(&mut layout_nodes, self.scale_factor);

                self.bencher.measure("Post-Layout");

                // Process output file and remove from redis
                page_id = self.process_layout_file(file.graph_id, &layout_nodes, &edges)?;
                self.redis.delete_layout_result(&graph_file.id)
            })
            .map_err(|e| format!("{:?}", e))?;

        info!("Sending message to dago-sequence-service...");
        SequencerClient::new()
            .map_err(|e| e.into())
            .and_then(|client| {
                // Send process request to dago-sequence-service
                client.process_file(page_id as u64)?;

                info!("... successfully processed by dago-sequence-service.");
                Ok(())
            })
            .map_err(|e: Box<dyn std::error::Error>| e.into())
    }

    /// Stores the nodes and edges in a quad tree structure, creates a new page for the given
    /// graph in the database and stores all nodes and edges for all tree levels as well.
    fn process_layout_file(
        &mut self,
        graph_id: i64,
        nodes: &HashMap<u64, GenericNode>,
        edges: &LinkedList<GenericEdge>,
    ) -> BoxResult<i64> {
        self.bencher.start();

        let tree = create_and_fill_tree(nodes, edges)?;
        self.bencher.measure("QuadTree creation");

        info!("Created quad tree with depth = {}", tree.max_depth);
        self.bencher
            .log(&format!("QuadTree Depth : {}", tree.max_depth));

        debug!("{}\n", tree.pretty_print());
        self.bencher.start();

        let page_id = self.create_new_page(graph_id, tree.max_depth)?;

        for depth in 1..=tree.max_depth {
            self.store_quad_tree_at_depth(page_id, depth, &tree, nodes)?;
            info!(
                "Successfully stored quad tree at depth {} / {}",
                depth, tree.max_depth
            );
        }

        self.bencher.measure("QuadTree storage");
        info!("Successfully processed layout file");
        Ok(page_id)
    }

    /// Creates a new page for the given graph and stores it in the database.
    fn create_new_page(&self, graph_id: i64, max_depth: u8) -> BoxResult<i64> {
        let n_pages = self.psql.get_num_graph_pages(graph_id).unwrap();
        debug!("Found {} pages for graph {}", n_pages, graph_id);

        let page = Page::new(-1, graph_id, n_pages + 1, max_depth as i16);
        let page_id = self.psql.insert_graph_page(&page)?;
        info!("Created new graph page with id {}", page_id);

        Ok(page_id)
    }

    /// Stores all nodes and links of the quad tree in the database
    fn store_quad_tree_at_depth(
        &self,
        page_id: i64,
        depth: u8,
        tree: &QuadTree,
        nodes: &HashMap<u64, GenericNode>,
    ) -> BoxResult<()> {
        let nodes_at_depth = tree.get_nodes_of_depth(depth);
        let node_id_map = self.store_nodes_at_depth(page_id, depth, &nodes_at_depth, nodes)?;

        let links_at_depth = tree.get_links_of_depth(depth)?;
        self.store_links_at_depth(page_id, depth, &links_at_depth, node_id_map)?;

        Ok(())
    }

    /// Puts all given nodes into the database and returns a hash map with node quad tree IDs as
    /// keys and node position IDs returned from the database as values.
    fn store_nodes_at_depth(
        &self,
        page_id: i64,
        depth: u8,
        nodes_at_depth: &Vec<SimpleQuadNode>,
        nodes: &HashMap<u64, GenericNode>,
    ) -> BoxResult<HashMap<u64, i64>> {
        let mut node_id_map = HashMap::new();

        for simple_node in nodes_at_depth.iter() {
            let position = Position {
                id: -1,
                x: simple_node.x,
                y: simple_node.y,
            };

            let mut attribute = Attribute::new(
                -1,
                simple_node.node_id.map(|id| id as i64),
                simple_node.id as i64,
            );

            // If simple_node has a simple_node-id (true for leaf nodes) and if the respective simple_node has
            // attributes, add them to the previously created attribute element.
            let mut attributes = &String::from("{}");
            if let Some(v_id) = simple_node.node_id {
                let node = nodes.get(&v_id);

                match node {
                    Some(node) => {
                        if node.attributes.is_some() {
                            attributes = node.attributes.as_ref().unwrap();
                        } else {
                            debug!("Node with id = {} does not have any attributes", v_id);
                        }
                    }
                    None => warn!("Could not find any node with id = {}", v_id),
                }
            }
            attribute.set_attributes(attributes);

            let mut node = Node {
                id: -1,
                page_id,
                depth: depth as i16,
                leaf: simple_node.is_leaf,
                position_id: -1,
                attribute_id: -1,
            };

            match self.psql.insert_node(&mut node, &position, &attribute) {
                Ok(_) => {
                    node_id_map.insert(simple_node.id, node.id);
                }
                _ => warn!("Could not store {:?}", node),
            };
        }

        Ok(node_id_map)
    }

    /// Puts all given links into the database.
    fn store_links_at_depth(
        &self,
        page_id: i64,
        depth: u8,
        links_at_depth: &Vec<(u64, u64)>,
        node_id_map: HashMap<u64, i64>,
    ) -> BoxResult<()> {
        for link in links_at_depth.iter() {
            let start_id = node_id_map.get(&link.0);
            let end_id = node_id_map.get(&link.1);

            match (start_id, end_id) {
                (Some(start_id), Some(end_id)) => {
                    let mut edge = Edge {
                        id: -1,
                        page_id,
                        depth: depth as i16,
                        start_node_id: *start_id,
                        end_node_id: *end_id,
                        attribute_id: 0,
                    };

                    let _ = self
                        .psql
                        .insert_edge(&mut edge, &Attribute::default())
                        .map_err(|e| warn!("Could not store {:?}: {}", link, e));
                }
                _ => warn!("Could not find positions for link {:?}", link),
            }
        }

        Ok(())
    }
}

/// Creates and returns a new quad tree containing all given valid nodes and edges.
/// Invalid nodes and edges are ignores.
fn create_and_fill_tree(
    nodes: &HashMap<u64, GenericNode>,
    edges: &LinkedList<GenericEdge>,
) -> BoxResult<QuadTree> {
    // Calculate required area of the quad tree
    let (x_min, x_max, y_min, y_max) = calc_area_of_nodes(&nodes);
    debug!(
        "Layout file area: {}, {}, {}, {}",
        x_min, x_max, y_min, y_max
    );

    let mut tree = QuadTree::new(x_min, x_max, y_min, y_max);

    // Add nodes to the quad tree
    for (_i, node) in nodes.iter() {
        match (node.x, node.y) {
            (Some(x), Some(y)) => tree.add_node(x, y, node.id).unwrap_or_else(|e| {
                warn!("Could not add node ({}, {}) to the quad tree: {}", x, y, e)
            }),
            _ => warn!("Invalid node {:?}", node.id),
        }
    }

    // Add edges to the quad tree
    for edge in edges.iter() {
        let start = nodes.get(&edge.start_id);
        let end = nodes.get(&edge.end_id);

        match (start, end) {
            (Some(start), Some(end)) => match (start.x, start.y, end.x, end.y) {
                (Some(start_x), Some(start_y), Some(end_x), Some(end_y)) => {
                    let _ = tree.add_link(start_x, start_y, end_x, end_y).map_err(|e| {
                        warn!("Could not add link {:?} to the quad tree: {}", edge, e)
                    });
                }
                _ => warn!("Edge is missing data {:?}, {:?}", start, end),
            },
            _ => warn!("Invalid edge {:?}", edge),
        }
    }

    debug!("Successfully created tree of depth {}, containing {} nodes and {} edges on the lowest level",
           tree.max_depth, tree.n_nodes, tree.n_links);
    Ok(tree)
}

/// Returns the size of the smallest square still covering all nodes.
fn calc_area_of_nodes(nodes: &HashMap<u64, GenericNode>) -> (f32, f32, f32, f32) {
    let mut x_min = f32::MAX;
    let mut x_max = f32::MIN;
    let mut y_min = f32::MAX;
    let mut y_max = f32::MIN;

    for (_i, v) in nodes.iter() {
        if let (Some(x), Some(y)) = (v.x, v.y) {
            x_min = f32::min(x_min, x);
            x_max = f32::max(x_max, x);
            y_min = f32::min(y_min, y);
            y_max = f32::max(y_max, y);
        }
    }

    (x_min, x_max, y_min, y_max)
}

/// Swaps the attributes of source-nodes with the attributes of target-nodes.
fn swap_attributes(target: &mut HashMap<u64, GenericNode>, source: &mut HashMap<u64, GenericNode>) {
    for (key, v_source) in source.iter_mut() {
        if let Some(v_target) = target.get_mut(key) {
            std::mem::swap(&mut v_target.attributes, &mut v_source.attributes);
        } else {
            warn!(
                "Could not find target-node with id={} to swap attributes",
                key
            );
        }
    }
}

/// Scale the graph size by a given factor
fn scale_layout(nodes: &mut HashMap<u64, GenericNode>, scale_factor: u16) {
    info!("Scaling layout by factor {}", scale_factor);
    for (_, node) in nodes.iter_mut() {
        if node.x.is_some() {
            node.x = Some(node.x.unwrap() * f32::from(scale_factor));
        }
        if node.y.is_some() {
            node.y = Some(node.y.unwrap() * f32::from(scale_factor));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn calc_area_of_nodes() {
        let mut nodes = Box::new(HashMap::new());
        nodes.insert(
            196017u64,
            GenericNode {
                id: 196017,
                x: Option::Some(-0.204402),
                y: Option::Some(1.),
                attributes: Option::None,
            },
        );
        nodes.insert(
            3886u64,
            GenericNode {
                id: 3886,
                x: Option::Some(0.),
                y: Option::Some(0.951723),
                attributes: Option::None,
            },
        );
        nodes.insert(
            503093u64,
            GenericNode {
                id: 503093,
                x: Option::Some(1.),
                y: Option::Some(-0.0493011),
                attributes: Option::None,
            },
        );
        nodes.insert(
            44984u64,
            GenericNode {
                id: 44984,
                x: Option::Some(0.796404),
                y: Option::Some(0.),
                attributes: Option::None,
            },
        );

        let (x_min, x_max, y_min, y_max) = super::calc_area_of_nodes(&nodes);
        assert_eq!(-0.204402, x_min);
        assert_eq!(1., x_max);
        assert_eq!(-0.0493011, y_min);
        assert_eq!(1., y_max);
    }

    #[test]
    fn test_swap_attributes() {
        let mut parsed_node_1 = GenericNode::new(1);
        parsed_node_1.attributes = Some(String::from(
            r#"{"label":"node 1","thisIsASampleAttribute":"42","anotherAttributeRightOverHere":"attr1"}"#,
        ));
        let mut parsed_node_2 = GenericNode::new(2);
        parsed_node_2.attributes = Some(String::from(
            r#"{"label":"node 2","thisIsASampleAttribute":"43","basicallyAnAttribute":["-17.12","Hi there!","-1.69"]}"#,
        ));
        let parsed_node_3 = GenericNode::new(3);

        let mut parsed_nodes: HashMap<u64, GenericNode> = HashMap::new();
        parsed_nodes.insert(1, parsed_node_1);
        parsed_nodes.insert(2, parsed_node_2);
        parsed_nodes.insert(3, parsed_node_3);

        let mut layouted_node_1 = GenericNode::new(1);
        layouted_node_1.x = Some(1.);
        layouted_node_1.y = Some(-1.);
        let mut layouted_node_2 = GenericNode::new(2);
        layouted_node_2.x = Some(2.);
        layouted_node_2.y = Some(-2.);
        let mut layouted_node_3 = GenericNode::new(3);
        layouted_node_3.x = Some(3.);
        layouted_node_3.y = Some(-3.);

        let mut layouted_nodes: HashMap<u64, GenericNode> = HashMap::new();
        layouted_nodes.insert(3, layouted_node_3);
        layouted_nodes.insert(2, layouted_node_2);
        layouted_nodes.insert(1, layouted_node_1);

        swap_attributes(&mut layouted_nodes, &mut parsed_nodes);

        assert_eq!(parsed_nodes.get(&1).unwrap().attributes, None);
        assert_eq!(parsed_nodes.get(&2).unwrap().attributes, None);
        assert_eq!(parsed_nodes.get(&3).unwrap().attributes, None);

        assert_eq!(layouted_nodes.get(&1).unwrap().x, Some(1.));
        assert_eq!(layouted_nodes.get(&1).unwrap().y, Some(-1.));
        assert_eq!(
            layouted_nodes.get(&1).unwrap().attributes,
            Some(String::from(
                r#"{"label":"node 1","thisIsASampleAttribute":"42","anotherAttributeRightOverHere":"attr1"}"#
            ))
        );
        assert_eq!(layouted_nodes.get(&2).unwrap().x, Some(2.));
        assert_eq!(layouted_nodes.get(&2).unwrap().y, Some(-2.));
        assert_eq!(
            layouted_nodes.get(&2).unwrap().attributes,
            Some(String::from(
                r#"{"label":"node 2","thisIsASampleAttribute":"43","basicallyAnAttribute":["-17.12","Hi there!","-1.69"]}"#
            ))
        );
        assert_eq!(layouted_nodes.get(&3).unwrap().x, Some(3.));
        assert_eq!(layouted_nodes.get(&3).unwrap().y, Some(-3.));
        assert_eq!(layouted_nodes.get(&3).unwrap().attributes, None);
    }
}
