use std::sync::Arc;

use grpcio::{ChannelBuilder, EnvBuilder};

use grpc::create_client_url;
use grpc::layout_file::LayoutFileRequest;
use grpc::layout_file_grpc::ProcessFileServiceClient;
use std::error::Error;
use BoxResult;

static ENV_LAYOUT_HOST: &str = "LAYOUT_HOST";
static ENV_LAYOUT_PORT: &str = "LAYOUT_PORT";

pub struct LayoutClient {
    client: ProcessFileServiceClient,
}

impl LayoutClient {
    pub fn new() -> BoxResult<Self> {
        let url = create_client_url(ENV_LAYOUT_HOST, ENV_LAYOUT_PORT)?;

        let env = Arc::new(EnvBuilder::new().build());
        let ch = ChannelBuilder::new(env).connect(url.as_str());
        let client = ProcessFileServiceClient::new(ch);

        Ok(LayoutClient { client })
    }

    pub fn process_file(&self, id: &str) -> BoxResult<()> {
        let mut req = LayoutFileRequest::new();
        req.set_id(id.to_string());
        self.client
            .process(&req)
            .map_err(|e| e.into())
            .and_then(|res| {
                if res.get_success() {
                    debug!("Sent process message to dago-layout.");
                    Ok(())
                } else {
                    Err("Dago-layout returned an error.".into())
                }
            })
            .map_err(|e: Box<dyn Error>| {
                error!("{}", e);
                e.into()
            })
    }
}
