use BoxResult;

use std::env;

pub mod empty;
pub mod layout_file;
pub mod layout_file_grpc;
pub mod process_file;
pub mod process_file_grpc;
pub mod sequence_page;
pub mod sequence_page_grpc;

pub mod client_layout;
pub mod client_sequencer;

fn create_client_url(host: &str, port: &str) -> BoxResult<String> {
    let host = env::var(host)?;
    let port = env::var(port)?;

    Ok(format!("{}:{}", host, port))
}
