use std::sync::Arc;

use grpcio::{ChannelBuilder, EnvBuilder};

use grpc::create_client_url;
use grpc::sequence_page::SequencePageRequest;
use grpc::sequence_page_grpc::SequencePageServiceClient;
use std::error::Error;
use BoxResult;

static ENV_SEQUENCER_HOST: &str = "SEQUENCER_HOST";
static ENV_SEQUENCER_PORT: &str = "SEQUENCER_PORT";

pub struct SequencerClient {
    client: SequencePageServiceClient,
}

impl SequencerClient {
    pub fn new() -> BoxResult<Self> {
        let url = create_client_url(ENV_SEQUENCER_HOST, ENV_SEQUENCER_PORT)?;

        let env = Arc::new(EnvBuilder::new().build());
        let ch = ChannelBuilder::new(env).connect(url.as_str());
        let client = SequencePageServiceClient::new(ch);

        Ok(SequencerClient { client })
    }

    pub fn process_file(&self, id: u64) -> BoxResult<()> {
        let mut req = SequencePageRequest::new();
        req.set_page_id(id);
        self.client
            .process(&req)
            .map_err(|e| e.into())
            .and_then(|res| {
                if res.get_result() {
                    debug!("Sent process message to dago-sequencer.");
                    Ok(())
                } else {
                    Err("Dago-sequencer returned an error.".into())
                }
            })
            .map_err(|e: Box<dyn Error>| {
                error!("{}", e);
                e.into()
            })
    }
}
