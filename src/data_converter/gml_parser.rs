use std::collections::{HashMap, LinkedList};
use std::str::{FromStr, Lines};

use super::{GenericEdge, GenericNode};

/// Tests if the given file has the structure of a Graph Model Language file.
pub fn is_parsable(file: &str) -> bool {
    let mut lines = file.lines();

    if let Some(line) = lines.next() {
        if line.trim().starts_with("graph") {
            return true;
        }
    }

    false
}

/// Parse content in the format of Graph Model Language into lists of individual nodes and edges.
pub fn parse(file: &str) -> (HashMap<u64, GenericNode>, LinkedList<GenericEdge>) {
    let mut nodes = HashMap::new();
    let mut edges = LinkedList::new();

    let mut lines = file.lines();

    loop {
        match lines.next() {
            Some(line) if line.trim().starts_with("node") => {
                parse_node(&mut lines, &mut nodes);
            }
            Some(line) if line.trim().starts_with("edge") => {
                parse_edge(&mut lines, &mut edges);
            }
            Some(_) => continue,
            None => break,
        }
    }

    (nodes, edges)
}

/// Process an individual node.
fn parse_node(lines: &mut Lines, nodes: &mut HashMap<u64, GenericNode>) {
    let mut id = Option::None;
    let mut attrs = object! {};

    loop {
        match lines.next() {
            Some(line) if line.trim().starts_with("id") => {
                let parts: Vec<&str> = line.split(" ").collect::<Vec<&str>>();
                if parts.len() == 2 {
                    id = parse_key_value(line);
                }
            }
            Some(line) if line.trim().starts_with("]") => break,
            Some(line) => {
                let parts: Vec<&str> = line.splitn(2, " ").collect::<Vec<&str>>();
                if parts.len() == 2 {
                    let clean_string =
                        |s: &str| -> String { s.replace("\"", "").replace("\t", "") };

                    let key = clean_string(parts[0]);
                    let value = clean_string(parts[1]);

                    if attrs.has_key(&key) {
                        let current_values = &attrs[&key];
                        if current_values.is_array() {
                            attrs[&key]
                                .push(value)
                                .or_else(|_| -> Result<(), json::JsonError> {
                                    warn!("Could not parse all attributes");
                                    Ok(())
                                })
                                .unwrap();
                        } else {
                            attrs[key] = array![current_values.to_string(), value];
                        }
                    } else {
                        attrs[key] = value.into();
                    }
                }
            }
            None => break,
        }
    }

    if id.is_some() {
        let mut node = GenericNode::new(id.unwrap());
        if attrs.len() > 0 {
            node.attributes = Some(attrs.dump());
        }

        nodes.insert(id.unwrap(), node);
    } else {
        warn!("Could not parse node");
    }
}

/// Process a individual edge.
fn parse_edge(lines: &mut Lines, edges: &mut LinkedList<GenericEdge>) {
    let mut start: Option<u64> = Option::None;
    let mut end: Option<u64> = Option::None;

    loop {
        match lines.next() {
            Some(line) if line.trim().starts_with("source") => {
                start = parse_key_value(line);
            }
            Some(line) if line.trim().starts_with("target") => {
                end = parse_key_value(line);
            }
            Some(line) if line.trim().starts_with("]") => break,
            Some(_) => continue,
            None => break,
        }
    }

    if start.is_some() && end.is_some() {
        let edge = GenericEdge {
            start_id: start.unwrap(),
            end_id: end.unwrap(),
        };
        edges.push_back(edge);
    } else {
        warn!("Could not parse edge");
    }
}

/// Parse the value of a key-value pair separated by a space.
fn parse_key_value<T: FromStr>(key_value: &str) -> Option<T> {
    let parts: Vec<&str> = key_value.split(" ").collect::<Vec<&str>>();
    if parts.len() == 2 {
        match T::from_str(parts[1]) {
            Ok(val) => Option::Some(val),
            Err(_) => Option::None,
        }
    } else {
        Option::None
    }
}

#[cfg(test)]
mod tests {
    use super::super::tests::read_test_file;
    use super::*;

    #[test]
    fn file_type() {
        let gml_file = read_test_file(String::from("gml.txt")).unwrap();
        let mm_file = read_test_file(String::from("mm.txt")).unwrap();
        let csv_file = read_test_file(String::from("csv.txt")).unwrap();

        assert!(is_parsable(&gml_file));
        assert!(!is_parsable(&mm_file));
        assert!(!is_parsable(&csv_file));
    }

    #[test]
    fn parse_gml() {
        let file = read_test_file(String::from("gml.txt")).unwrap();
        let (nodes, mut edges) = parse(&file);
        assert_eq!(3, nodes.len());
        assert_eq!(3, edges.len());

        let n_0 = nodes.get(&1).unwrap();
        assert_eq!(1, n_0.id);
        assert!(n_0.x.is_none());
        assert!(n_0.y.is_none());
        assert_eq!(n_0.attributes.as_ref().unwrap(), r#"{"label":"node 1","thisIsASampleAttribute":"42","anotherAttributeRightOverHere":"attr1"}"#);
        let n_1 = nodes.get(&2).unwrap();
        assert_eq!(2, n_1.id);
        assert!(n_1.x.is_none());
        assert!(n_1.y.is_none());
        assert_eq!(n_1.attributes.as_ref().unwrap(), r#"{"label":"node 2","thisIsASampleAttribute":"43","basicallyAnAttribute":["-17.12","Hi there!","-1.69"]}"#);
        let n_2 = nodes.get(&3).unwrap();
        assert_eq!(3, n_2.id);
        assert!(n_2.x.is_none());
        assert!(n_2.y.is_none());
        assert!(n_2.attributes.is_none());

        let e_0 = edges.pop_front().unwrap();
        assert_eq!(1, e_0.start_id);
        assert_eq!(2, e_0.end_id);
        let e_1 = edges.pop_front().unwrap();
        assert_eq!(2, e_1.start_id);
        assert_eq!(3, e_1.end_id);
        let e_2 = edges.pop_front().unwrap();
        assert_eq!(3, e_2.start_id);
        assert_eq!(1, e_2.end_id);
    }
}
