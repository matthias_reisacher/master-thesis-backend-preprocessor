use std::collections::{HashMap, LinkedList};
use std::str::FromStr;

use super::{GenericEdge, GenericNode};

/// Tests if the given file has the structure of a CSV file.
pub fn is_parsable(file: &str) -> bool {
    let mut lines = file.lines();

    if let Some(line) = lines.next() {
        let parts: Vec<&str> = line.trim().split(";").collect::<Vec<&str>>();
        if parts.len() >= 2 {
            return true;
        }
    }

    false
}

/// Parse a list of edges in the CSV format into lists of individual nodes and edges.
pub fn parse(file: &str) -> (HashMap<u64, GenericNode>, LinkedList<GenericEdge>) {
    let mut nodes = HashMap::new();
    let mut edges = LinkedList::new();

    let mut lines = file.lines();

    loop {
        match lines.next() {
            Some(line) => parse_line(line.trim(), &mut nodes, &mut edges),
            None => break,
        }
    }

    (nodes, edges)
}

/// Processes an individual line of the CSV-file.
fn parse_line(
    line: &str,
    nodes: &mut HashMap<u64, GenericNode>,
    edges: &mut LinkedList<GenericEdge>,
) {
    let parts: Vec<&str> = line.split(";").collect::<Vec<&str>>();
    if parts.len() >= 2 {
        if let (Ok(start), Ok(end)) = (u64::from_str(parts[0]), u64::from_str(parts[1])) {
            process_node(start, nodes);
            process_node(end, nodes);
            process_edge(start, end, edges);
        } else {
            warn!("Could not parse edge '{}'", line);
        }
    }
}

/// Create a new node if not already existing with the specified ID.
fn process_node(id: u64, nodes: &mut HashMap<u64, GenericNode>) {
    if !nodes.contains_key(&id) {
        let node = GenericNode::new(id);
        nodes.insert(id, node);
    }
}

/// Create a new edge.
fn process_edge(start_id: u64, end_id: u64, edges: &mut LinkedList<GenericEdge>) {
    let edge = GenericEdge { start_id, end_id };
    edges.push_back(edge);
}

#[cfg(test)]
mod tests {
    use super::super::tests::read_test_file;
    use super::*;

    #[test]
    fn file_type() {
        let csv_file = read_test_file(String::from("csv.txt")).unwrap();
        let gml_file = read_test_file(String::from("gml.txt")).unwrap();
        let mm_file = read_test_file(String::from("mm.txt")).unwrap();

        assert!(is_parsable(&csv_file));
        assert!(!is_parsable(&gml_file));
        assert!(!is_parsable(&mm_file));
    }

    #[test]
    fn parse_mm() {
        let file = read_test_file(String::from("csv.txt")).unwrap();
        let (nodes, mut edges) = parse(&file);

        assert_eq!(3, nodes.len());
        assert_eq!(3, edges.len());

        let n_0 = nodes.get(&1).unwrap();
        assert_eq!(1, n_0.id);
        assert!(n_0.x.is_none());
        assert!(n_0.y.is_none());
        assert!(n_0.attributes.is_none());
        let n_1 = nodes.get(&2).unwrap();
        assert_eq!(2, n_1.id);
        assert!(n_1.x.is_none());
        assert!(n_1.y.is_none());
        assert!(n_1.attributes.is_none());
        let n_2 = nodes.get(&3).unwrap();
        assert_eq!(3, n_2.id);
        assert!(n_2.x.is_none());
        assert!(n_2.y.is_none());
        assert!(n_2.attributes.is_none());

        let e_0 = edges.pop_front().unwrap();
        assert_eq!(1, e_0.start_id);
        assert_eq!(2, e_0.end_id);
        let e_1 = edges.pop_front().unwrap();
        assert_eq!(2, e_1.start_id);
        assert_eq!(3, e_1.end_id);
        let e_2 = edges.pop_front().unwrap();
        assert_eq!(3, e_2.start_id);
        assert_eq!(1, e_2.end_id);
    }

    #[test]
    fn parse_line_with_missing_semicolon_ending() {
        let mut nodes: HashMap<u64, GenericNode> = HashMap::new();
        let mut edges: LinkedList<GenericEdge> = LinkedList::new();

        parse_line("1;2", &mut nodes, &mut edges);
        assert_eq!(2, nodes.len());
        assert_eq!(1, edges.len());
    }
}
