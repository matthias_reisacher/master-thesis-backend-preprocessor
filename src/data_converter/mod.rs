use std::collections::{HashMap, LinkedList};

use BoxResult;

mod csv_parser;
mod gml_parser;
mod layout_input_builder;
mod layout_output_parser;
mod mm_parser;

/// Tries to parse the given file with an appropriate parser depending on the file structure and
/// returns the containing nodes and edges if successful.
pub fn parse_input_file(
    file: &str,
) -> BoxResult<(HashMap<u64, GenericNode>, LinkedList<GenericEdge>)> {
    if csv_parser::is_parsable(file) {
        info!("Input file is of type CSV");
        return Ok(csv_parser::parse(file));
    }

    if gml_parser::is_parsable(file) {
        info!("Input file is of type Graph Model Language");
        return Ok(gml_parser::parse(file));
    }

    if mm_parser::is_parsable(file) {
        info!("Input file is of type Matrix Market");
        return Ok(mm_parser::parse(file));
    }

    Err("Could not find appropriate parser for the given file".into())
}

/// Creates a file which is parsable by the dago-layout-service containing the given nodes and
/// edges.
pub fn create_layout_input_file(
    nodes: &HashMap<u64, GenericNode>,
    edges: &LinkedList<GenericEdge>,
) -> String {
    layout_input_builder::build_from_set(nodes, edges)
}

/// Parses the layout file created by the layout micro service.
pub fn parse_layout_file(file: &str) -> HashMap<u64, GenericNode> {
    layout_output_parser::parse(file)
}

#[derive(Debug)]
pub struct GenericNode {
    pub id: u64,
    pub x: Option<f32>,
    pub y: Option<f32>,
    pub attributes: Option<String>, // Attributes in JSON format
}

impl GenericNode {
    /// Creates a new generic node.
    pub fn new(id: u64) -> Self {
        GenericNode {
            id,
            x: Option::None,
            y: Option::None,
            attributes: Option::None,
        }
    }
}

#[derive(Debug)]
pub struct GenericEdge {
    pub start_id: u64,
    pub end_id: u64,
}

#[cfg(test)]
pub mod tests {
    use std::{fs, path::PathBuf};

    pub fn read_test_file(filename: String) -> Result<String, String> {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push(format!("resources/test/{}", filename));
        fs::read_to_string(path).map_err(|e| e.to_string())
    }
}
