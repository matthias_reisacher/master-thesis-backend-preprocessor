use std::collections::{HashMap, LinkedList};

use data_converter::{GenericEdge, GenericNode};

/// Builds the content of a layout input file containing all nodes and edges.
#[allow(dead_code)]
pub fn build_from_list(nodes: &LinkedList<GenericNode>, edges: &LinkedList<GenericEdge>) -> String {
    let mut file = create_header();

    for node in nodes.iter() {
        file.push_str(&add_node(node));
    }

    for edge in edges.iter() {
        file.push_str(&add_edge(edge));
    }

    file.trim().to_string()
}

/// Builds the content of a layout input file containing all nodes and edges.
pub fn build_from_set(
    nodes: &HashMap<u64, GenericNode>,
    edges: &LinkedList<GenericEdge>,
) -> String {
    let mut file = create_header();

    for (_i, node) in nodes.iter() {
        file.push_str(&add_node(node));
    }

    for edge in edges.iter() {
        file.push_str(&add_edge(edge));
    }

    file.trim().to_string()
}

/// Creates a string containing the two header lines of the layout input file.
fn create_header() -> String {
    String::from("BOTH\tBOTH\tNODE\tEDGE\tEDGE\tNODE\nSTRING\tSTRING\tINT\tINT\tINT\tSTRING\nOperation\tType\tNodeId\tFrom\tTo\tVType\n")
}

/// Creates a string containing the add-node operator for the given node.
fn add_node(node: &GenericNode) -> String {
    let mut s = String::from("add\tNode\t");
    s.push_str(&node.id.to_string());
    s.push_str("\n");
    s
}

/// Creates a string containing the add-edge operator for the given edge.
fn add_edge(edge: &GenericEdge) -> String {
    let mut s = String::from("add\tEdge\t\t");
    s.push_str(&edge.start_id.to_string());
    s.push_str("\t");
    s.push_str(&edge.end_id.to_string());
    s.push_str("\n");
    s
}

#[cfg(test)]
mod tests {
    use super::super::tests::read_test_file;
    use super::*;

    #[test]
    fn build_layout_input_from_list() {
        let mut nodes = LinkedList::new();
        nodes.push_back(GenericNode::new(196017));
        nodes.push_back(GenericNode::new(3886));
        nodes.push_back(GenericNode::new(503093));
        nodes.push_back(GenericNode::new(44984));

        let mut edges = LinkedList::new();
        edges.push_back(GenericEdge {
            start_id: 196017,
            end_id: 3886,
        });
        edges.push_back(GenericEdge {
            start_id: 503093,
            end_id: 44984,
        });

        let file = build_from_list(&nodes, &edges);
        let layout_input = read_test_file(String::from("layout_input.txt")).unwrap();
        assert_eq!(layout_input, file);
    }

    #[test]
    fn build_layout_input_from_set() {
        let mut nodes = HashMap::new();
        nodes.insert(196017, GenericNode::new(196017));
        nodes.insert(3886, GenericNode::new(3886));
        nodes.insert(503093, GenericNode::new(503093));
        nodes.insert(44984, GenericNode::new(44984));

        let mut edges = LinkedList::new();
        edges.push_back(GenericEdge {
            start_id: 196017,
            end_id: 3886,
        });
        edges.push_back(GenericEdge {
            start_id: 503093,
            end_id: 44984,
        });

        let file = build_from_set(&nodes, &edges);
        let layout_input = read_test_file(String::from("layout_input.txt")).unwrap();
        assert_eq!(layout_input.len(), file.len());

        for line in layout_input.lines() {
            assert!(file.contains(line));
        }
    }
}
