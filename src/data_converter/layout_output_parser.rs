use std::collections::HashMap;
use std::f32;
use std::str::FromStr;

use super::GenericNode;

/// Parse the output of the layout-microservice into a map of nodes.
pub fn parse(file: &str) -> HashMap<u64, GenericNode> {
    let mut nodes = HashMap::new();

    let mut lines = file.lines();

    loop {
        match lines.next() {
            Some(line) => {
                parse_line(line, &mut nodes);
            }
            None => break,
        }
    }

    nodes
}

/// Processes an individual line and creates a new node if successful.
fn parse_line(line: &str, nodes: &mut HashMap<u64, GenericNode>) {
    let mut node = Option::None;

    let parts: Vec<&str> = line.split(";").collect::<Vec<&str>>();
    if parts.len() == 3 {
        if let (Ok(id), Ok(x), Ok(y)) = (
            u64::from_str(parts[0]),
            f32::from_str(parts[1]),
            f32::from_str(parts[2]),
        ) {
            node = Some(GenericNode {
                id,
                x: Option::Some(x),
                y: Option::Some(y),
                attributes: Option::None,
            });
        }
    }

    if node.is_some() {
        nodes.insert(node.as_ref().unwrap().id, node.unwrap());
    } else {
        warn!("Could not parse node '{}'", line);
    }
}

#[cfg(test)]
mod tests {
    use super::super::tests::read_test_file;
    use super::*;

    #[test]
    fn parse_layout_output() {
        let file = read_test_file(String::from("layout_output.txt")).unwrap();
        let nodes = parse(&file);
        assert_eq!(4, nodes.len());

        let node_1 = nodes.get(&196017).unwrap();
        assert_eq!(196017, node_1.id);
        assert_eq!(0.204402, node_1.x.unwrap());
        assert_eq!(1.0, node_1.y.unwrap());

        let node_2 = nodes.get(&3886).unwrap();
        assert_eq!(3886, node_2.id);
        assert_eq!(0.0, node_2.x.unwrap());
        assert_eq!(0.951723, node_2.y.unwrap());

        let node_3 = nodes.get(&503093).unwrap();
        assert_eq!(503093, node_3.id);
        assert_eq!(1.0, node_3.x.unwrap());
        assert_eq!(0.0493011, node_3.y.unwrap());

        let node_4 = nodes.get(&44984).unwrap();
        assert_eq!(44984, node_4.id);
        assert_eq!(0.796404, node_4.x.unwrap());
        assert_eq!(0.0, node_4.y.unwrap());
    }
}
