use std::collections::{HashMap, LinkedList};
use std::str::FromStr;

use super::{GenericEdge, GenericNode};

/// Tests if the given file has the structure of a MatrixMarket file.
pub fn is_parsable(file: &str) -> bool {
    let mut lines = file.lines();

    loop {
        match lines.next() {
            Some(line) if line.trim().starts_with("%") => {}
            Some(line) => {
                let parts: Vec<&str> = line.split(" ").collect::<Vec<&str>>();

                if parts.len() == 3 {
                    return true;
                } else {
                    return false;
                }
            }
            None => return false,
        }
    }
}

/// Parse content in the format of MatrixMarket into lists of individual nodes and edges.
pub fn parse(file: &str) -> (HashMap<u64, GenericNode>, LinkedList<GenericEdge>) {
    let mut nodes = HashMap::new();
    let mut edges = LinkedList::new();

    let mut lines = file.lines();
    let mut header_processed = false;

    loop {
        match lines.next() {
            Some(line) if line.trim().starts_with("%") => {}
            Some(line) => {
                // Skip the header line
                if !header_processed {
                    header_processed = true;
                } else {
                    parse_line(line, &mut nodes, &mut edges);
                }
            }
            None => break,
        }
    }

    (nodes, edges)
}

/// Processes an individual line and creates a new node if successful.
fn parse_line(
    line: &str,
    nodes: &mut HashMap<u64, GenericNode>,
    edges: &mut LinkedList<GenericEdge>,
) {
    match parse_nodes(line) {
        Some((start, end)) => {
            process_node(start, nodes);
            process_node(end, nodes);
            process_edge(start, end, edges);
        }
        None => warn!("Could not parse edge"),
    }
}

/// Create a new edge.
fn process_edge(start_id: u64, end_id: u64, edges: &mut LinkedList<GenericEdge>) {
    let edge = GenericEdge { start_id, end_id };
    edges.push_back(edge);
}

/// Create a new node if not already existing with the specified ID.
fn process_node(id: u64, nodes: &mut HashMap<u64, GenericNode>) {
    if !nodes.contains_key(&id) {
        let node = GenericNode::new(id);
        nodes.insert(id, node);
    }
}

/// Parse single line and returns the node data.
/// Note: The weight of the edge (3. value) is ignored!
fn parse_nodes<T: FromStr>(key_value: &str) -> Option<(T, T)> {
    let mut s_e = Option::None;

    let parts: Vec<&str> = key_value.split(" ").collect::<Vec<&str>>();
    if parts.len() >= 2 {
        if let (Ok(start), Ok(end)) = (T::from_str(parts[0]), T::from_str(parts[1])) {
            s_e = Option::Some((start, end));
        }
    }

    s_e
}

#[cfg(test)]
mod tests {
    use super::super::tests::read_test_file;
    use super::*;

    #[test]
    fn file_type() {
        let mm_file = read_test_file(String::from("mm.txt")).unwrap();
        let csv_file = read_test_file(String::from("csv.txt")).unwrap();
        let gml_file = read_test_file(String::from("gml.txt")).unwrap();

        assert!(is_parsable(&mm_file));
        assert!(!is_parsable(&csv_file));
        assert!(!is_parsable(&gml_file));
    }

    #[test]
    fn parse_mm() {
        let file = read_test_file(String::from("mm.txt")).unwrap();
        let (nodes, mut edges) = parse(&file);

        let n_0 = nodes.get(&1).unwrap();
        assert_eq!(1, n_0.id);
        assert!(n_0.x.is_none());
        assert!(n_0.y.is_none());
        assert!(n_0.attributes.is_none());
        let n_1 = nodes.get(&2).unwrap();
        assert_eq!(2, n_1.id);
        assert!(n_1.x.is_none());
        assert!(n_1.y.is_none());
        assert!(n_1.attributes.is_none());
        let n_2 = nodes.get(&1761).unwrap();
        assert_eq!(1761, n_2.id);
        assert!(n_2.x.is_none());
        assert!(n_2.y.is_none());
        assert!(n_2.attributes.is_none());
        let n_3 = nodes.get(&7097).unwrap();
        assert_eq!(7097, n_3.id);
        assert!(n_3.x.is_none());
        assert!(n_3.y.is_none());
        assert!(n_3.attributes.is_none());

        let e_0 = edges.pop_front().unwrap();
        assert_eq!(1, e_0.start_id);
        assert_eq!(1, e_0.end_id);
        let e_1 = edges.pop_front().unwrap();
        assert_eq!(1, e_1.start_id);
        assert_eq!(2, e_1.end_id);
        let e_2 = edges.pop_front().unwrap();
        assert_eq!(2, e_2.start_id);
        assert_eq!(2, e_2.end_id);
        let e_3 = edges.pop_front().unwrap();
        assert_eq!(1761, e_3.start_id);
        assert_eq!(2, e_3.end_id);
        let e_4 = edges.pop_front().unwrap();
        assert_eq!(7097, e_4.start_id);
        assert_eq!(2, e_4.end_id);
    }
}
